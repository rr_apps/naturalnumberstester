﻿-->  	Natural Numbers Tester
-->		Created by: David Teren
-->		Created on: 09/27/13 13:14:22
-->		Updated on: 6-Oct-2014
-->		Copyright © 2013 Radio Retail
-->		All Rights Reserved
-->		Description: 	Natural Numbers Tester is used to verify that the Values where recorded correctly.
-->						The script will first verify that the required dependincies are in place and that  			
-->						the "NN_PR_Tests" folder exists and contains the reqired files.
--> 					A timestamped text file is output listing passed and failed values.

--------------------------------------------------------------------------------------------------------------------------------------------------

-- display dialog theRoot
--> This defines the location of the folder "NN_PR_Tests" containing the audio files to be tested based on the computer name.
set theRoot to path to me


--"OSX Dev:Users:Shared:Dropbox:03_Developement:02_Projects:1_RadioRetail:NN_PR_Tests"
--set theRoot to "Volumes:Projects:NN_PR_Tests:"
--set theRoot to "Macintosh HD:Users:Shared:NN_PR_Tests:"
set theRoot to "Volumes:Projects:NN_PR_Tests:"

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Set the folder locations
set theProfiles to theRoot & "01_Profiles"
set theMusic to theRoot & "02_Music:"
set theReports to theRoot & "03_Reports"
set theOutputs to theRoot & "04_Outputs"
set theTemp to quoted form of (POSIX path of theRoot & "05_Temp/")
set thetemp2 to theRoot & "05_Temp"
set thetemp3 to theRoot & "05_Temp"
set spacer to " "

-- display dialog " test : " & theOutputs
--------------------------------------------------------------------------------------------------------------------------------------------------

--> Clear the temp folder of previous files
set totalTempFiles to (do shell script "ls -1 " & theTemp & " | wc -l") as integer
try
	if totalTempFiles > 0 then
		do shell script "rm -r -- " & theTemp & "*"
	end if
end try

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Set Music Volume
set musicVol to "1.5"

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Set session date
set d to current date
set yr to year of d as string
set mth to (month of d as number) as string
if length of mth is 1 then set mth to "0" & mth
set dy to day of d as string
if length of dy is 1 then set dy to "0" & dy
set h to hours of d as string
if length of h is 1 then set h to "0" & h
set m to minutes of d as string
if length of m is 1 then set m to "0" & m
set s to seconds of d as string
if length of s is 1 then set s to "0" & s
set sessionDate to yr & mth & dy & "_" & h & "-" & m & "-" & s as string

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Set File Names
set joinedFile to "01Joined"
set paddedMusic to "/paddedmusic"
set outFile to "03OutPut"

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Check for dependencies see if libraries and apps exist.
set isSoxThere to (do shell script "[ -f /opt/local/bin/sox ] && echo \"Found\" || echo \"Not Found\"")
set SoXLocation to "/opt/local/bin/"
if isSoxThere is "Not Found" then
	set isSoxThere2 to (do shell script "[ -f /usr/local/bin/sox ] && echo \"Found\" || echo \"Not Found\"")
	set SoXLocation to "/usr/local/bin/"
	if isSoxThere2 is "Not Found" then
		display dialog "The required SoX library was not found. Please install SoX by installing MacPorts (http://www.macports.org/)"
		set SoXLocation to ""
	end if
end if

--> Set tool locations
set SoX to SoXLocation & "sox"
set SoXi to SoXLocation & "soxi"
set PlaY to SoXLocation & "play"
set EchO to "/bin/echo"

set resourcePath to fileStuff((POSIX path of (path to me)), 4) -- Get the "Resources" directory path.
set SoXProc to (quoted form of resourcePath & "/libs/soxproc")




--> Check for the "soxproc" bash script. This script is used for batch processing. In this case to trim silences.
--set isSoXProcThere to (do shell script "[ -f /Users/Shared/Copy/Dev.ShellScripts/bin/soxproc ] && echo \"Found\" || echo \"Not Found\"")
--set SoXProc to "/Users/Shared/Copy/Dev.ShellScripts/bin/soxproc"
--if isSoXProcThere is "Not Found" then
--	set isSoXProcThere2 to (do shell script "[ -f /usr/local/bin/soxproc ] && echo \"Found\" || echo \"Not Found\"")
--	if isSoXProcThere2 is "Not Found" then
--		set thePass to text returned of (display dialog "WARNING! A required process was not found and can be downloaded and installed. Please Enter Your Password:" default answer "")
--		do shell script "cd /usr/local/bin/ ; sudo curl -O https://dl.dropboxusercontent.com/u/15689446/RadioRetail/dependencies/SoX/soxproc" password thePass with administrator privileges
--		set SoXProc to "/usr/local/bin/soxproc"
--	end if
-- end if

--> Check to see if "Play Sound app" is in the Applications folder.
set msg to "no"
tell application "Finder" to if exists "Mavericks:Applications:Play Sound.app" then set msg to "yes"
--if msg is "no" then
--	display dialog " Play Sound application not found in the Applications folder. Please download and install to that location. (http://microcosmsoftware.com/playsound/)"
-- end if

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Menu to choose the voice profile
set mainList to {"anton_eng", "anton_afr", "charl_eng", "charl_afr", "christinew_eng", "christinew_afr", "cintaine_eng", "cintaine_afr", "gazz_eng", "hugo_eng", "hugo_afr", "jordan_eng", "kerri_eng", "liselle_eng", "liselle_afr", "sean_eng", "sindi_eng", "____________________________"}
choose from list mainList with prompt "Choose Voice Profile To Process"
set theProfile to result as text

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Select the final file format
set outputExt to ".wav"

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Create Date stamped folder in the Output folder for the created files
set theOutPutFolder to POSIX path of theOutputs & "/" & theProfile & "_" & sessionDate
set theReport to POSIX path of theReports & "/" & theProfile & "_" & sessionDate & "_Report.txt"
do shell script "mkdir " & theOutPutFolder

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Set the folder aliases
set currencyFolder to theProfiles & ":" & theProfile & ":curr:" as alias
set oldPBFolder to theProfiles & ":" & theProfile & ":oldpb:" as alias
set PBFolder to theProfiles & ":" & theProfile & ":pb:" as alias
set valOneFolder to theProfiles & ":" & theProfile & ":valone:" as alias
set valTwoFolder to theProfiles & ":" & theProfile & ":valtwo:" as alias
set tempPath to theTemp

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Read the CSV profiles and set as variables
set CSV_File to (theProfiles & ":" & theProfile & ".csv") as text
set CSV_Lines to read file CSV_File using delimiter {return}
set AppleScript's text item delimiters to ";"
set VoiceArtist to last text item of item 1 of CSV_Lines
set spokenLang to last text item of item 2 of CSV_Lines
set padValueZero to last text item of item 3 of CSV_Lines
set padValueOne to last text item of item 4 of CSV_Lines
set padValueTwo to last text item of item 5 of CSV_Lines
set padValueThree to last text item of item 6 of CSV_Lines

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Get the file count in the folders
set totalCurrency to ((do shell script "find " & quoted form of POSIX path of currencyFolder & " \\! -name '.*' | wc -l") as integer) - 1
set totalPB to ((do shell script "find " & quoted form of POSIX path of PBFolder & " \\! -name '.*' | wc -l") as integer) - 1
set totalOldPB to ((do shell script "find " & quoted form of POSIX path of oldPBFolder & " \\! -name '.*' | wc -l") as integer) - 1
set totalValOne to ((do shell script "find " & quoted form of POSIX path of valOneFolder & " \\! -name '.*' | wc -l") as integer) - 1
set totalValTwo to ((do shell script "find " & quoted form of POSIX path of valTwoFolder & " \\! -name '.*' | wc -l") as integer) - 1
set totalMusic to ((do shell script "find " & quoted form of POSIX path of theMusic & " \\! -name '.*' | wc -l") as integer) - 1

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Set the paths to the WAV files
set currPath to (POSIX path of currencyFolder)
set valOnePath to (POSIX path of valOneFolder)
set valTwoPath to (POSIX path of valTwoFolder)
set musicPath to (POSIX path of theMusic)
set pBPath to (POSIX path of PBFolder)
set soxTempPath to (POSIX path of thetemp3)

-------------------------------------------------------------------------------------------------------------------------------------------------

-->  Determine the file ext (File format)
# tell application "Finder"
# 	set currencyExt to "." & name extension of file 1 of currencyFolder
# 	set pbExt to "." & name extension of file 1 of PBFolder
# 	if totalOldPB > 0 then
# 		set oldpbExt to "." & name extension of file 1 of oldPBFolder
# 	end if
# 	set valOneExt to "." & name extension of file 1 of valOneFolder
# 	set valTwoExt to "." & name extension of file 1 of valTwoFolder
# 	set musicExt to "." & name extension of file 1 of theMusic
# end tell

set currencyExt to ".wav"
set pbExt to ".wav"
if totalOldPB > 0 then
	set oldpbExt to ".wav"
end if
set valOneExt to ".wav"
set valTwoExt to ".wav"
set musicExt to ".wav"

--------------------------------------------------------------------------------------------------------------------------------------------------

-->  Trim the Ends
display dialog "Would Like To Trim The Silence At The File Ends?" buttons {"Yes", "No"} default button 2
if the button returned of the result is "Yes" then
	display dialog "Please Wait While Files Are Trimmed!" buttons {"Ok"}
	set theProcess to "silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0"
	set AppleScript's text item delimiters to ""
	set trimExt to trim_line(currencyExt, ".", 0)
	do shell script SoXProc & " " & (quoted form of currPath) & " " & trimExt & " " & theProcess
	
	set trimExt to trim_line(valOneExt, ".", 0)
	do shell script SoXProc & " " & valOnePath & " " & trimExt & " " & theProcess
	
	set trimExt to trim_line(valTwoExt, ".", 0)
	do shell script SoXProc & " " & valTwoPath & " " & trimExt & " " & theProcess
	
	set trimExt to trim_line(pbExt, ".", 0)
	do shell script SoXProc & " " & pBPath & " " & trimExt & " " & theProcess
	
	display dialog "File Trimming Complete!" buttons {"Ok"}
else
	-- action for 2nd button goes here
end if

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Error with content if any of the folders are empty - Display an error message.
try
	if totalCurrency is 0 then
		display dialog "Error: 
		Please check the file contents.
		There are " & totalCurrency & " Currency Files." buttons {"OK"}
		error
	else if totalPB is 0 then
		display dialog "Error: 
		Please check the file contents. 
		There are " & (totalPB) & " PB Files." buttons {"OK"}
		error
	else if totalValOne is 0 then
		display dialog "Error: 
		Please check the file contents. 
		There are " & totalValOne & " Value One Files." buttons {"OK"}
		error
	else if totalValTwo is 0 then
		display dialog "Error: 
		Please check the file contents. 
		There are " & totalValTwo & " Value Two Files." buttons {"OK"}
		error
	else if totalMusic is 0 then
		display dialog "Error: 
		Please check the file contents. 
		There are " & totalMusic & " Music Files" buttons {"OK"}
		error
	end if
on error
	return
end try

--------------------------------------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------------------------------

--> File Names for Padding
set filePadZero to "padzero" & outputExt
set filePadOne to "padone" & outputExt
set filePadTwo to "padtwo" & outputExt
set filePadThree to "padthree" & outputExt

--> Routine to force comma to point
set dd to character 2 of (0.0 as text)
set v to padValueZero as text
set old_delims to AppleScript's text item delimiters
set AppleScript's text item delimiters to dd
set l to every text item of v
set AppleScript's text item delimiters to "."
set v to l as text
set AppleScript's text item delimiters to old_delims

--> Routine to force comma to point
set dd to character 2 of (0.0 as text)
set x to padValueOne as text
set old_delims to AppleScript's text item delimiters
set AppleScript's text item delimiters to dd
set l to every text item of x
set AppleScript's text item delimiters to "."
set x to l as text
set AppleScript's text item delimiters to old_delims

--> Routine to force comma to point
set dd to character 2 of (0.0 as text)
set y to padValueTwo as text
set old_delims to AppleScript's text item delimiters
set AppleScript's text item delimiters to dd
set l to every text item of y
set AppleScript's text item delimiters to "."
set y to l as text
set AppleScript's text item delimiters to old_delims

--> Routine to force comma to point
set dd to character 2 of (0.0 as text)
set z to padValueThree as text
set old_delims to AppleScript's text item delimiters
set AppleScript's text item delimiters to dd
set l to every text item of z
set AppleScript's text item delimiters to "."
set z to l as text
set AppleScript's text item delimiters to old_delims

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Create pad files 
do shell script SoX & " -n -b 16 -c 2 -r 44100 " & tempPath & filePadZero & " trim 0.0 " & v ---- Silent Pad that goes between the PB and the virst value
do shell script SoX & " -n -b 16 -c 2 -r 44100 " & tempPath & filePadOne & " trim 0.0 " & x ---- Silent Pad that goes between the virst value and the currency
do shell script SoX & " -n -b 16 -c 2 -r 44100 " & tempPath & filePadTwo & " trim 0.0 " & y ---- Silent Pad that goes between the currency and the second value
do shell script SoX & " -n -b 16 -c 2 -r 44100 " & tempPath & filePadThree & " trim 0.0 " & z ---- Silent Pad that goes between second value and the second part of the PB

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Define the pad extension
set padExt to outputExt

--> Set currency counter to zero
set rCurr to 0

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Repeat the Process and Count [This is still buggy as when the count is started a certain values the countDown results in a non compliant value.
set counTer to text returned of (display dialog "Start From: " default answer "1")

repeat
	set countDown to 100 - (counTer mod 99)
	set CountSet to 0
	
	if counTer is 99 then
		set countDown to 99
		set CountSet to 1
	end if
	
	if counTer is 198 then
		set countDown to countDown - 1
		set CountSet to 5
	end if
	
	if counTer is 297 then
		set countDown to countDown - 1
		set CountSet to 6
	end if
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	--> Random Value Generators & Counters
	
	--> Random PB Selector
	set maxValuePB to totalPB
	set thePB to (random number from 1 to maxValuePB)
	if thePB mod 2 is not equal to 1 then -- Odd numbers only
		set thePB to thePB - 1
	end if
	set thePBTwo to thePB + 1
	set soxPB to (pBPath & zero_pad(thePB, 3) & pbExt & spacer)
	set soxPBTwo to (pBPath & zero_pad(thePBTwo, 3) & pbExt & spacer)
	
	--> Define value one
	set rVO to counTer
	set soxValOne to (valOnePath & zero_pad(rVO, 3) & valOneExt & spacer)
	
	--> Currency counter
	set rCurr to rCurr + 1
	set theCurrency to zero_pad(rCurr, 3)
	set soxCurr to (currPath & zero_pad(rCurr, 3) & currencyExt & spacer)
	
	---- > ValueTwo Selector 
	set rVT to countDown
	set soxValTwo to (valTwoPath & zero_pad(rVT, 3) & valTwoExt & spacer)
	
	---- Random Music Selector
	set maxMusic to totalMusic
	set rMus to (random number from 1 to maxMusic)
	set theMusic to zero_pad(rMus, 3)
	set soxMus to (musicPath & zero_pad(rMus, 3) & musicExt & spacer)
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	set soxPadZero to soxTempPath & "/padzero" & padExt & spacer
	set soxPadOne to soxTempPath & "/padone" & padExt & spacer
	set soxPadTwo to soxTempPath & "/padtwo" & padExt & spacer
	set soxPadThree to soxTempPath & "/padthree" & padExt & spacer
	
	
	set concatFile to POSIX path of (thetemp3 & ":concated" & padExt)
	set playFile to (thetemp3 & ":concated" & padExt)
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	-- set soxProcess to (POSIX path of SoX & spacer & soxPB & soxPadZero & soxValOne & soxPadOne & soxCurr & soxPadTwo & soxValTwo & soxPadThree & soxPBTwo & soxPadZero & soxValOne & soxPadOne & soxCurr & soxPadTwo & soxValTwo & " -c 2 " & concatFile & " pad 2 2")
	
    	set soxProcess to (POSIX path of SoX & spacer & soxPB & soxPadZero & soxValOne &  soxPadTwo & soxValTwo & soxPadThree & soxPBTwo & soxPadZero & soxValOne & soxPadTwo & soxValTwo & " -c 2 " & concatFile & " pad 2 2")
    
	
	do shell script soxProcess
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	---- Determine length of Advert
	set aFile to concatFile
	set AppleScript's text item delimiters to " = "
	set duration to text item 1 of (do shell script SoXi & " -D " & aFile)
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	set soxMusPadded to soxTempPath & paddedMusic & outputExt
	
	--> Trim the Music to match the Advert length and add in/out fades
	do shell script SoX & spacer & "-v " & musicVol & spacer & soxMus & soxMusPadded & " trim 0 " & duration & " fade h 0:3 0 0:3"
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	--> Mix the advert & the music files
	set theFinalFile to soxTempPath & "/outputFile" & outputExt
    -- do shell script SoX & " -m " & concatFile & spacer & soxMusPadded & spacer & theFinalFile
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
    
    ## Mastering -- These settings can be tweaked once the system goes live for optimal results.
    # The mastering chain is as follows;
    # 	Gain reduction to avoid clipping --> Compression (an artifact is the attack is fast so this results in a fade-in) --> EQ with boost in the lows and highs --> Normalisation

    --if masterSwitch is "on" then
    	set audioGain to "gain -10"
    	-- set audioComp to "compand 0,0.1 -60,-60,-30,-15,-20,-12,-4,-8,-2,-7 -2"¬
    	set audioComp to "compand 0,0.2 -90,-90,-70,-70,-60,-30,-2,-8,-2,-7 -2"
    	set audioEQ to "equalizer 40 .71q +12 equalizer 80 1.10q +0 equalizer 240 1.80q -3 equalizer 500 .71q +0 equalizer 1000 2.90q +0 equalizer 4100 .51q +2.5 equalizer 8500 .71q +2.0 equalizer 17000 .71q +6"
    	set audioNorm to "norm"
    -- else
--         set audioGain to "gain 0"
--         set audioComp to ""
--         set audioEQ to ""
--         set audioNorm to ""
--         set audioSinc to ""
--         set audioRate to ""
--     end if

    ## Telephone Emulation -- These are for test purposes to better evaluate the results.
    # The emulation chain is as follows;
    # High pass filters at 120Hz & low pass filters at 3400Hz

    -- if telEmu is "on" then
    	set audioSinc to "sinc 90-12000"
        -- set audioRate to "-r 8k"
    -- else
 --        set audioSinc to ""
 --        set audioRate to ""
 --    end if


    # Set the audio processing chain
    set audioProc to audioGain & space & audioComp & space & audioEQ & space & audioSinc & space & audioNorm

    ## Set the command that SoX will run
    -- set audioCmd to sOX & space & concatFile & space & audioRate & space & masteredFile & space & audioProc

    # Run the actual process using SoX to concatenate the parts 
    -- do shell script audioCmd
    
    
    
    do shell script SoX & " -m " & concatFile & spacer & soxMusPadded & spacer & theFinalFile & spacer & audioProc
    
    
    
    
    
    
    
    
	
	--> The Short Play Routine. Usefull if you only want to hear the last part of the test file.
	--if thisComp is "Studio Two's Mac Pro" then
	--	set theShort to soxTempPath & "/outputFile2" & outputExt
	--	do shell script SoX & " " & theFinalFile & " " & theShort & " reverse trim 0 00:05.5 reverse"
	--	set theFinalFile to theShort
	--end if
	
	-------------------------------------------------------------------------------------------------------------------------------------------------
	
	--> Start the Playback (Note: This requires the Play Sound application http://microcosmsoftware.com/playsound/)
	set finalPlay to POSIX file theFinalFile as Unicode text
	tell application "Play Sound"
		play finalPlay repeat 0
	end tell
	
	--------------------------------------------------------------------------------------------------------------------------------------------------	
	
	
	--> Options Menu 
	set extOptions to {"All Passed - Continue", "---------------------", "ValOne Failed - Continue", "---------------------", "ValTwo Failed - Continue", "---------------------", "ValOne & ValTwo Failed - Continue", "---------------------", "Repeat Last Played Values", "---------------------", "Currency Passed - Continue", "---------------------", "Currency Failed - Continue", "---------------------", "Exit", "--------------------------------------------------------------"}
	choose from list extOptions with prompt "PLAYING: " & " Value One: " & rVO & "  Value Two: " & rVT & "          Currency: " & rCurr & "
	"
	set fileManagement to result as text
	
	if fileManagement is "ValOne Failed - Continue" then
		do shell script EchO & " ValOne_" & rVO & " - Failed" & " >> " & theReport
	else if the fileManagement is "ValTwo Failed - Continue" then
		do shell script EchO & " ValTwo_" & rVT & " - Failed" & " >> " & theReport
	else if the fileManagement is "ValOne & ValTwo Failed - Continue" then
		do shell script EchO & " ValOne_" & rVO & " - Failed" & " >> " & theReport
		do shell script EchO & " ValTwo_" & rVT & " - Failed" & " >> " & theReport
	else if fileManagement is "All Passed - Continue" then
		do shell script EchO & " ValOne_" & rVO & " ValTwo_" & rVT & " - Passed" & " >> " & theReport
		
		
	else if fileManagement is "Repeat Last Played Values" then
		set counTer to counTer - 1
		set rCurr to rCurr - 1
		
		
	else if fileManagement is "---------------------" then
		set counTer to counTer - 1
		set rCurr to rCurr - 1
		
	else if fileManagement is "Currency Passed - Continue" then
		do shell script EchO & " Currency: " & rCurr & " - Passed" & " >> " & theReport
		
	else if fileManagement is "Currency Failed - Continue" then
		do shell script EchO & " Currency: " & rCurr & " - Failed" & " >> " & theReport
		
		
	else if fileManagement is "Exit" then
		tell application "Play Sound"
			stop finalPlay
		end tell
		set totalTempFiles to ((do shell script "find " & theTemp & " \\! -name '.*' | wc -l") as integer) - 1
		try
			if totalTempFiles > 0 then
				do shell script "rm -r -- " & theTemp & "*"
			end if
		end try
		error number -128
	else if fileManagement is "Exit & Delete Created Files" then
		do shell script "rm -rf " & quoted form of (theOutPutFolder)
		tell application "Play Sound"
			stop finalPlay
		end tell
		set totalTempFiles to ((do shell script "find " & theTemp & " \\! -name '.*' | wc -l") as integer) - 1
		try
			if totalTempFiles > 0 then
				do shell script "rm -r -- " & theTemp & "*"
			end if
		end try
		error number -128
	else if fileManagement is "Exit & Delete Reports" then
		
		--do shell script "rm -rf " & quoted form of (theReport)
		tell application "Play Sound"
			stop finalPlay
		end tell
		set totalTempFiles to ((do shell script "find " & theTemp & " \\! -name '.*' | wc -l") as integer) - 1
		try
			if totalTempFiles > 0 then
				do shell script "rm -r -- " & theTemp & "*"
			end if
		end try
		error number -128
		
		
	else if fileManagement is "Exit & Delete Both Files & Reports" then
		do shell script "rm -rf " & quoted form of (theOutPutFolder)
		--do shell script "rm -rf " & quoted form of (theReport)
		tell application "Play Sound"
			stop finalPlay
		end tell
		set totalTempFiles to ((do shell script "find " & theTemp & " \\! -name '.*' | wc -l") as integer) - 1
		try
			if totalTempFiles > 0 then
				do shell script "rm -r -- " & theTemp & "*"
			end if
		end try
		error number -128
		
	else if fileManagement is "Exit & Delete All Reports & Created Files" then
		display dialog "WARNING!: ARE YOU SURE YOU WANT TO DELETE ALL REPORTS AND CREATED FILES?"
		do shell script "rm -rf " & quoted form of (POSIX path of theOutputs)
		do shell script "rm -rf " & quoted form of (POSIX path of theReports)
		do shell script "mkdir " & quoted form of (POSIX path of theOutputs)
		do shell script "mkdir " & quoted form of (POSIX path of theReports)
		tell application "Play Sound"
			stop finalPlay
		end tell
		set totalTempFiles to ((do shell script "find " & theTemp & " \\! -name '.*' | wc -l") as integer) - 1
		try
			if totalTempFiles > 0 then
				do shell script "rm -r -- " & theTemp & "*"
			end if
		end try
		error number -128
		
	else
		
		tell application "Play Sound"
			stop finalPlay
		end tell
		
		error number -128
	end if
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	-->  Stop the Playback	
	tell application "Play Sound"
		stop finalPlay
	end tell
	
	--------------------------------------------------------------------------------------------------------------------------------------------------
	
	if rCurr ≥ totalCurrency then
		set rCurr to 0
	end if
	
	set counTer to counTer + 1
	
	if counTer is 301 then
		error number -128 -- use this to end the script
	end if
	
end repeat

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Clear the temp folder of previous files
set totalTempFiles to ((do shell script "find " & theTemp & " \\! -name '.*' | wc -l") as integer) - 1
try
	if totalTempFiles > 0 then
		do shell script "rm -r -- " & theTemp & "*"
	end if
end try

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Zero Padding Subroutine
on zero_pad(value, string_length)
	set string_zeroes to ""
	set digits_to_pad to string_length - (length of (value as string))
	if digits_to_pad > 0 then
		repeat digits_to_pad times
			set string_zeroes to string_zeroes & "0" as string
		end repeat
	end if
	set padded_value to string_zeroes & value as string
	return padded_value
end zero_pad

--------------------------------------------------------------------------------------------------------------------------------------------------

--> Character trimmer. Used to trim file name and determine the extension.
on trim_line(this_text, trim_chars, trim_indicator)
	-- 0 = beginning, 1 = end, 2 = both
	set x to the length of the trim_chars
	-- TRIM BEGINNING
	if the trim_indicator is in {0, 2} then
		repeat while this_text begins with the trim_chars
			try
				set this_text to characters (x + 1) thru -1 of this_text as string
			on error
				-- the text contains nothing but the trim characters
				return ""
			end try
		end repeat
	end if
	-- TRIM ENDING
	if the trim_indicator is in {1, 2} then
		repeat while this_text ends with the trim_chars
			try
				set this_text to characters 1 thru -(x + 1) of this_text as string
			on error
				-- the text contains nothing but the trim characters
				return ""
			end try
		end repeat
	end if
	return this_text
end trim_line

--------------------------------------------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------
-- #I4 - Get the file stuff.
-- Get the file name, ext, path or path alias.
-- by David Teren
--
---------------------------------------------------------------------
-- Get the file name, ext, path or path alias. 
on fileStuff(theFile, theOption)
	-- Option (1)thePosixFile (2)fileName (3)fileExt (4)filePath (5)filePathAlias
	set theResults to {}
	set thePosixFile to POSIX path of theFile
	set fileName to (do shell script "basename  " & quoted form of thePosixFile)
	set fileExt to (do shell script "fileExt=$(echo " & quoted form of thePosixFile & " | sed -e 's/.*\\.//') ; echo $fileExt")
	set filePath to (do shell script "dirname " & quoted form of POSIX path of thePosixFile)
	set filePathAlias to POSIX file (do shell script "dirname " & quoted form of POSIX path of thePosixFile) as alias
	set theResults to {thePosixFile, fileName, fileExt, filePath, filePathAlias}
	set theResult to item theOption of theResults
	return theResult
end fileStuff
